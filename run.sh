# customisations 
export run_ver=1
export log_file_name=benchmark-${SLURM_JOB_ID}-${SLURM_JOB_NAME}-${SLURM_JOB_PARTITION}-${SLURM_JOB_NODELIST}-${run_ver}.log
export example_to_run=doubleslit
export octopus_commit_hash=main
export mpsd_sft_ver=dev-23a
export toolchain_version=foss2022a-mpi
export toolchain_name=toolchains/${toolchain_version}
export toolchain_dir=$HOME/compo
# standard process (nothing below this needs to be changed)
# script_dir=${SLURM_SUBMIT_DIR}
script_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
# set variable mpsd_arch to environment variable MPSD_MICROARCH if present, otherwise to "unknown"
export mpsd_arch=${MPSD_MICROARCH:-UNKNOWN_MICROARCH}
# make a folder named ${run_ver}-${mpsd_arch} in the current directory
run_folder=${script_dir}/${run_ver}-${mpsd_arch}
mkdir -p ${run_folder}
# copy the required example folder to the run folder
cp -r examples/${example_to_run}/* ${run_folder}
# change to the run folder
cd ${run_folder}
# Log all possible information about the job to the log file
export log_file=${run_folder}/${log_file_name}
touch ${log_file}
echo "Job started at: $(date)" >> ${log_file}
echo "Job id: ${SLURM_JOB_ID}" >> ${log_file}
echo "Job name: ${SLURM_JOB_NAME}" >> ${log_file}
echo "Job partition: ${SLURM_JOB_PARTITION}" >> ${log_file}
echo "Job nodelist: ${SLURM_JOB_NODELIST}" >> ${log_file}
echo "Job run_ver: ${run_ver}" >> ${log_file}
echo "Job mpsd_arch: ${mpsd_arch}" >> ${log_file}
echo "Job example_to_run: ${example_to_run}" >> ${log_file}
echo "Job run_folder: ${run_folder}" >> ${log_file}
mpsd-show-job-resources >> ${log_file}
echo " Octopus commit hash: ${octopus_commit_hash}" >> ${log_file}
echo "#################### Building octopus" >> ${log_file}
#echo time since epoch: 
echo ">>KEYTIME-build_toolchain_start===$(date +%s)" >> ${log_file}
# Build toolchain.
cd ${toolchain_dir}
git clone git@gitlab.gwdg.de:mpsd-cs/mpsd-software-environments.git
cd mpsd-software-environments
git checkout 1-user-interface-for-top-level-install-command
./mpsd-software-environment.py install dev-23a --toolchains ${toolchain_version}
## Build octopus and log the build_time
# load toolchain
eval `/usr/share/lmod/lmod/libexec/lmod use ${toolchain_dir}/mpsd-software-environments/dev-23a/${mpsd_arch}/lmod/Core`
eval `/usr/share/lmod/lmod/libexec/lmod load ${toolchain_name}`
# log the toolchain
eval `/usr/share/lmod/lmod/libexec/lmod list` >> ${log_file}
which gcc >> ${log_file}
which mpicc >> ${log_file}
echo ">>KEYTIME-build_toolchain_end===$(date +%s)" >> ${log_file}
# clone octopus
echo ">>KEYTIME-build_start===$(date +%s)" >> ${log_file}
cd ${run_folder}
echo "# cloning octopus" >> ${log_file}
git clone https://gitlab.com/octopus-code/octopus.git
cd octopus
git checkout ${octopus_commit_hash}
echo "# running autoreconf" >> ${log_file}
autoreconf -i
mkdir _build 
cd _build
cp "${script_dir}/mpsd-config-wrapper.sh" .
echo "# running configure" >> ${log_file}
source mpsd-config-wrapper.sh --prefix="${run_folder}/octopus/_build/installed"
echo "# running make" >> ${log_file}
make -j 8
make install
echo ">>KEYTIME-build_end===$(date +%s)" >> ${log_file}
echo "#################### Building octopus done" >> ${log_file}
# run and log make check

# run example_to_run
echo "#################### Running ${example_to_run}" >> ${log_file}
echo ">>KEYTIME-run_start===$(date +%s)" >> ${log_file}
cd ${run_folder}
${run_folder}/octopus/_build/installed/bin/octopus 2>&1 | tee run.log
echo ">>KEYTIME-run_end===$(date +%s)" >> ${log_file}